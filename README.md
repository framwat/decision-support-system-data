# Decision Support System Data

## Data structure

### Postgis database

Database is in directory `data` compressed to `sql.gz` file. While starting new postgis container data is automatically imported

Postgis database is accessible with credentials:
host: `localhost`
port: `5432`
user: `frogis`
password: `This1Is2FroGIS`
database: `frogis`

### Geoserver

All geoserver configuration is in `geoserver` directory and is used while starging geoserver conatiner. After running geoserver is accessible from
url: http://localhost:8888/geoserver/index.html
user: `admin`
password: `m0RSKEbPF7ZdNu`

## Running services locally

`docker-compose up`
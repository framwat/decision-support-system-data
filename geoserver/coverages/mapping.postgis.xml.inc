 <spatialExtension name="postgis"/>
 <mapping>
      <masterTable name="mosaic" >
              <coverageNameAttribute name="name"/>
              <maxXAttribute name="maxx"/>
              <maxYAttribute name="maxy"/>
              <minXAttribute name="minx"/>
              <minYAttribute name="miny"/>
              <resXAttribute name="resx"/>
              <resYAttribute name="resy"/>
              <tileTableNameAtribute  name="tiletable" />
      </masterTable>
      <tileTable>
              <blobAttributeName name="rast" />
	<keyAtttributeName name="rid" />
      </tileTable>
</mapping>
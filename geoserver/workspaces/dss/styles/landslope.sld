<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" 
    xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
    xmlns="http://www.opengis.net/sld" 
    xmlns:ogc="http://www.opengis.net/ogc" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>Landslope</Name>
    <UserStyle>
      <Title>Landslope</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <ColorMap>
              <ColorMapEntry color="#f0ecaa" quantity="2" label="2" opacity="1"/>
              <ColorMapEntry color="#d6cc92" quantity="5.6" label="5.6" opacity="1"/>
              <ColorMapEntry color="#bdad7b" quantity="12" label="12" opacity="1"/>
              <ColorMapEntry color="#a69165" quantity="19" label="19" opacity="1"/>
         	  <ColorMapEntry color="#8f7651" quantity="28" label="28" opacity="1"/>
              <ColorMapEntry color="#7a5e40" quantity="39" label="39" opacity="1"/>
              <ColorMapEntry color="#664830" quantity="70" label="70" opacity="1"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>